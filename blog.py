#!/usr/bin/env python
'''
@author jstober
'''

import ConfigParser
import codecs

def list_init(arg = None):
    if arg:
        return arg.split(',')
    else:
        return []

class Blog(object):
    """
    Site specific parameters.
    """

    def __init__(self):
        self.proper_fields = (('title', unicode),
                              ('test_url', unicode),
                              ('prod_url', unicode),
                              ('local', unicode),
                              ('location', list_init),
                              ('draft', unicode),
                              ('templates', unicode),
                              ('ssh', unicode),
                              ('ga', unicode),
                              ('headers', list_init),
                              ('header_links', list_init),
                              ('author', unicode))
        for field, initializer in self.proper_fields:
            setattr(self, field, initializer())

    def __repr__(self):
        result = ""
        for field,_ in self.proper_fields:
            result += "{0}: {1}\n".format(field, getattr(self,field))
        result += "\nPlugins\n\n"
        for k,v in self.plugins.items():
            result += "{0}: {1}\n".format(k,v)
        return result

    def read_blog(self, filename):
        config = ConfigParser.ConfigParser()
        config.readfp(codecs.open(filename,encoding='utf-8'))
        # populate proper fields
        self.location = eval(config.get('blog','location'))
        self.title = config.get('blog','title')
        self.ssh = config.get('blog','ssh')
        self.author = config.get('blog','author')
        self.headers = eval(config.get('blog','headers'))
        self.header_links = dict(zip(self.headers,eval(config.get('blog','header_links')))) #ugh
        self.prod_url = config.get('blog','prod_url')
        self.test_url = config.get('blog','test_url')
        self.local = config.get('blog','local')
        self.templates = config.get('blog','templates')
        self.ga = config.get('blog','ga')
        self.draft = config.get('blog','draft')

        self.plugins = dict(config.items('plugins'))


if __name__ == '__main__':

    blog = Blog()
    blog.read_blog('blog.cfg')
    print blog
