#!/bin/bash -x

TS=`date '+%Y-%m-%d_%H:%M:%S'`
CONFIG="blog.cfg" # configures TARGET 
TARGET=`cat $CONFIG | grep ssh | cut -d' ' -f3`

#rsync -rzv _site/ $TARGET --exclude archives --backup --delete --backup-dir=archives/$TS
rsync -rzv _site/ $TARGET --delete 
rsync -v .htaccess $TARGET/archives/
