#!/usr/bin/env python
'''
@author jstober
'''

import codecs
import os
import datetime
import re
import sys
import uuid
import glob
import shutil
import pdb
from blog import Blog
from slug import slugify
from docutils.core import publish_parts
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter
from jinja2 import Template

def indent(s):
    """ Indent a multiline string. """
    return "   " + re.sub('\n', '\n   ', s, flags=re.M)

def unindent(s):
    """ Unindent a multiline string. """
    return re.sub('^   ', '', s, flags=re.M)

def strip_div(s):
    """ strip doc div """
    match = re.match(r'^<div class="document"(?:\s*id=".*?")?>(.*)</div>$',s,flags=re.M+re.U+re.DOTALL)
    if not match:
        print s
        return s
    return match.group(1)

def strip_title_class(s):
    """ strip class="title" """
    return re.sub(r'<h1 class="title"',r'<h1',s,flags=re.U)

def list_init(arg = None):
    if arg:
        return arg.split(',')
    else:
        return []

def dt_init(arg = None):
    if arg:

        try:
            return datetime.datetime.strptime(arg, "%Y-%m-%d %H:%M:%S")
        except:
            pass
        
        try:
            return datetime.datetime.strptime(arg, "%Y-%m-%d")
        except:
            pass

        return datetime.datetime(1900,1,1,0,0,0)

    else:
        return datetime.datetime.now()

def utc_init(arg = None):
    if arg:
        
        try:
            return datetime.datetime.strptime(arg, "%Y-%m-%d %H:%M:%S")
        except:
            pass
        
        try:
            return datetime.datetime.strptime(arg, "%Y-%m-%d")
        except:
            pass
    
        return datetime.datetime(1900,1,1,0,0,0)

    else:
        return datetime.datetime.utcnow()

def utf_init(arg = None):
    if arg is None:
        return u''
    else:
        return unicode(arg)

def int_init(arg = None):
    if arg is None:
        return -1
    else:
        return int(arg)

def id_init(arg=None):
    if arg is None:
        return str(uuid.uuid4())[:8]
    else:
        return arg

class Post(object):
    """
    Post class stores Python post data for individual posts. Knows how to serialize to/from a flat file representation of a post.
    """

    def __init__(self):
        self.proper_fields = (('title', utf_init), 
                              ('link', utf_init),
                              ('rel_link', utf_init),
                              ('shortlink', utf_init),
                              ('author', utf_init),
                              ('date', dt_init),
                              ('date_utc', utc_init),
                              ('categories', list_init),
                              ('tags', list_init),
                              ('id', id_init),
                              ('type', utf_init),
                              ('status', utf_init),
                              ('slug', utf_init),
                              ('format', utf_init),
                              ('filename', utf_init))
        for field, initializer in self.proper_fields:
            setattr(self,field,initializer())

        self.body = u''
        self.strict = True # restructuredtext compatability

    def metadata(self,name):
        """
        Write document header data.
        """
        if not hasattr(self,name): # is a warning required?
            return u':{0}: \n'.format(name)
        elif type(getattr(self,name)) == list:
            content = u','.join(getattr(self,name))
            return u':{0}: {1}\n'.format(name, content)
        elif type(getattr(self,name)) == datetime.datetime:
            content = getattr(self,name).strftime("%Y-%m-%d %H:%M:%S")
            return u':{0}: {1}\n'.format(name,content)
        else: # unicode or int
            return u':{0}: {1}\n'.format(name, getattr(self,name))

    def write_post(self, basedir = "", fullpath = ""):
        """
        Creates flat file db of rst structured posts.
        """
        filepath = ""

        if fullpath:
            filepath = fullpath
        else:
            filepath = os.path.join(basedir,self.filename)

        filepath = os.path.expanduser(filepath)
        try:
            fp = codecs.open(filepath,encoding="utf-8",mode="w")
        except IOError:
            # try creating the directory
            os.makedirs(basedir)
            fp = codecs.open(filepath,encoding="utf-8",mode="w")
            
        result = self.generate_unicode()
        fp.write(result)
        fp.close()

    def __repr__(self):
        return self.generate_unicode()

    def generate_unicode(self):
        result = u""
        for name,_ in self.proper_fields:
            result += self.metadata(name)
        result += "\n"

        if self.format == 'html' and self.strict:
            result += ".. raw:: html\n\n"
            result += indent(self.body)
        else:
            result += self.body
        return result

    def dict_post(self,post):
        """ Post provided in dictionary form. """
        for field, initializer in self.proper_fields:
            if post.has_key(field):
                setattr(self, field, initializer(post[field]))

    def gen_rel_link(self):
        self.rel_link = "{0}/{1}/".format(self.date.strftime("%Y/%m/%d"),self.slug)

    def read_post(self, filename, blog):
        """
        Read a post from a file.
        """
        fp = codecs.open(filename, encoding="utf-8")
        initializers = dict(self.proper_fields)

        for line in fp.readlines():
            match = re.match(r':(?P<docinfo>\S+):(?P<data>.*)',line,re.UNICODE)
            if match:
                docinfo = match.group('docinfo')
                init = initializers[docinfo]
                setattr(self, docinfo, init(match.group('data').strip()))
            else:
                self.body += line

        # Note that this overrides whatever may or may not be in the file.
        self.filename = os.path.split(filename)[1]

        if self.strict and self.format == 'html':
            # remove .. raw:: html and unindent
            self.body = re.sub(r'\n*\.\. raw:: html\n*', r'', self.body, re.M)
            self.body = unindent(self.body)
        else:
            self.body = self.body.strip()

        self.render(blog)

    # Regular expression used by wp-syntax
    # "/\s*<pre(?:lang=[\"']([\w-]+)[\"']|line=[\"'](\d*)[\"']|escaped=[\"'](true|false)?[\"']|highlight=[\"']((?:\d+[,-])*\d+)[\"']|src=[\"']([^\"']+)[\"']|\s)+>(.*)<\/pre>\s*/siU"

    def html_pygments(self, html):
        m = re.search('<pre lang="(.*?)">(.*?)</pre>',html,flags=re.M+re.U+re.DOTALL)   
        while m:
            name = m.group(1)
            code = m.group(2)
            if name == 'shell':
                name = 'bash' # hack for wp-syntax - pygments compatability
            lexer = get_lexer_by_name(name, stripall=True)
            formatter = HtmlFormatter(linenos=True, cssclass="source")
            result = highlight(code, lexer, formatter)
            html =  html[:m.start()] + result + html[m.end():]
            m = re.search('<pre lang="(.*?)">(.*?)</pre>',html,flags=re.M+re.U+re.DOTALL)   
        return html

    def html_uploads(self, html):
        """ Repoint html image sources to uploads folder of current blog. """
        pass

    def fix_chars(self, html):
        regexp = r'&(?!#\d+;|\w+;)' # find bar & that aren't part of entities
        html = re.sub(regexp,'&amp;',html)
        return html

    def render(self, blog):
        """
        Renders the post text into html.
        """
        if self.format == 'html':
            self.render_text = self.html_pygments(self.body)
            self.render_text = self.fix_chars(self.render_text)
            self.render_text = Template(self.render_text).render(blog=blog)
        else: # assume rst format
            self.raw_render_text = publish_parts(self.body,writer_name='html')['html_body']
            self.render_text = strip_title_class(strip_div(self.raw_render_text)).strip()

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Create a post file with some metadata fields.')
    parser.add_argument('--title', help='Title for post.') 
    parser.add_argument('--publish', action='store_true', help='Move published posts from drafts to real folder')
    args = parser.parse_args()
    blog = Blog()
    blog.read_blog('blog.cfg')


    if args.title:
        slug = slugify(args.title)
        date = datetime.datetime.now()
        utc_date = datetime.datetime.utcnow()


        post = Post()
        post.dict_post({ 'format': u'rst',
                         'title': args.title,
                         'slug': slug,
                         'filename': slug + ".rst",
                         'rel_link': "{0}/{1}/".format(date.strftime("%Y/%m/%d"),slug),
                         'shortlink': "",
                         'categories': [],
                         'tags': [],
                         'type': "post",
                         'status': "draft"})

        post.write_post(basedir = blog.draft)
    elif args.publish:
        pub_dir = blog.location[0]
        draft_dir = blog.draft
        drafts = glob.glob(os.path.expanduser(draft_dir) + "/*.rst")
        for d in drafts:
            post = Post()
            post.read_post(d,blog)
            if post.status == 'publish':
                print "Moving: ", d, pub_dir
                shutil.move(d, os.path.expanduser(pub_dir))
    else:
        raise ValueError, "Not enough arguments!"


if __name__ == '__main__':   
    main()    

