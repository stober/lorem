#!/usr/bin/env python
'''
@author jstober

Parse wordpress xml.
'''

#from xml.etree.ElementTree import parse
from lxml.etree import parse
from subprocess import Popen, PIPE, call
import cPickle as pickle
import re
import codecs
from post import Post
import datetime
import os.path
from slug import slugify

def rd(data):
    """ Deprecated - Formerly used to do unicode translation. """
    return data

nsmap = { 'excerpt' : "{http://wordpress.org/export/1.2/excerpt/}",
          'content': "{http://purl.org/rss/1.0/modules/content/}",
          'wfw': "{http://wellformedweb.org/CommentAPI/}",
          'dc': "{http://purl.org/dc/elements/1.1/}",
          'wp': "{http://wordpress.org/export/1.2/}" }

def nm(tag):
    """ Convert to {URI}Tag format. """
    if tag.find(':') > -1:
        prefix, suffix = tag.split(':')
        return "{0}{1}".format(nsmap[prefix],suffix)
    else:
        return tag

def get(elem, tag):
    try:
        return rd(elem.iter(nm(tag)).next().text)
    except:
        return ""

def cats(elem):
    return ",".join([item.attrib['nicename'] for item in elem.iter(nm('category')) if item.attrib['domain'] == 'category'])

def tags(elem):
    return ",".join([item.attrib['nicename'] for item in elem.iter(nm('category')) if item.attrib['domain'] == 'post_tag'])

def short(elem):
    for item in elem.iter(nm('wp:postmeta')):
        key = item.iter(nm('wp:meta_key')).next().text
        if key == '_yoast_bitlylink':
            return item.iter(nm('wp:meta_value')).next().text

def html2rst(bodytext):
    """
    Use pandoc to convert html to rst file. DEPRECATED.
    """
    pandoc = Popen(["/usr/local/bin/pandoc","--from=html","--to=rst"], stdin=PIPE, stdout=PIPE)
    return pandoc.communicate(bodytext)[0]

def convert_date(datestr):
    datetime.datetime.strptime("%Y")

def add_line_breaks(s):
    """ Call out to php to run wpautop. """
    wpautop = Popen(["php", "-f", "wpautop.php"], stdin=PIPE, stdout=PIPE)
    result = wpautop.communicate(s.encode('utf-8'))[0].decode('utf-8')
    return result

def revise_sources(html):
    matches = re.finditer(r'src\s*=\s*"\s*(http://(?:www\.)?depthfirstsearch.net/blog/wp-content/uploads)(.*?)"', html, flags=re.M+re.U+re.DOTALL)
    for m in matches:
        html = html[:m.start(1)] + "{{blog.url}}/uploads" + html[m.end(1):]
    matches = re.finditer(r'href\s*=\s*"\s*(http://(?:www\.)?depthfirstsearch.net/blog/wp-content/uploads)(.*?)"', html, flags=re.M+re.U+re.DOTALL)
    for m in matches:
        html = html[:m.start(1)] + "{{blog.url}}/uploads" + html[m.end(1):]
    
    return html

def populate_post_data(item):
    post = Post()

    post.dict_post({ 'format': u'html',
                     'title': get(item,"title"),
                     'link': get(item,"link"),
                     'shortlink': short(item),
                     'id': get(item,"wp:post_id"),
                     'slug': get(item, "wp:post_name"),
                     'categories': cats(item),
                     'tags': tags(item),
                     'type': get(item,"wp:post_type"),
                     'date_utc': get(item, "wp:post_date_gmt"),
                     'date': get(item,"wp:post_date"),
                     'status': get(item,"wp:status")})

    post.slug = gen_slug(post) # fix missing slug
    post.filename = gen_name(post) # generate missing filename
    post.body = add_line_breaks(get(item,"content:encoded"))
    post.body = revise_sources(post.body)
    post.gen_rel_link()

    return post

uniq_names = set([])
uniq_ids = set([])

def is_int(val):
    try:
        int(val)
        return True
    except:
        return False

def gen_slug(post):
    if (post.slug == '' or is_int(post.slug)) and len(post.title) > 0:
        return unicode(slugify(post.title))
    elif post.slug == '' and post.title == '':
        return unicode(post.id)
    else: # can't do anything
        return post.slug

def gen_name(post):
    name = ""

    global uniq_names
    global uniq_ids
        
    if post.slug and (not post.slug in uniq_names):
        uniq_names.add(post.slug)
        name = "{0}.rst".format(post.slug)

    elif post.slug and (not post.id in uniq_ids):
        uniq_names.add(post.slug + "-" + str(post.id))
        name = "{0}-{1}.rst".format(post.slug,post.id)

    # elif not post.id in uniq_ids:
    #     uniq_names.add("noname-" + str(post.id))
    #     name = "{0}-{1}.rst".format("noname",post.id)

    else:
        print post
        raise ValueError, "Unable to generate a unique name!"

    return name

def publish_log(basedir):
    fp = open(basedir + "wordpress.log", "w")
    fp.write("\n".join(uniq_names))
    fp.close()

def item_iterator(dom, post_type = 'post'):
    for item in dom.iter('item'):
        if post_type == get(item,"wp:post_type"):
            yield item

def export_posts(dom, loc="posts/export", with_log=False):
    for item in item_iterator(dom, post_type = 'post'):
        post = populate_post_data(item)

        if post.status == 'draft':
            post.write_post(loc + "/draft")
        else:
            post.write_post(loc + "/publish")

    if with_log:
        publish_log("posts/export/")    

def export_attachments(dom, loc="attachments/export"):
    attachments = set()
    for item in item_iterator(dom,post_type='attachment'):
        attachments.add(get(item,'guid'))
    return attachments

def search_attachments_posts(dom):
    attachments = set()
    for item in item_iterator(dom,post_type='post'):
        content = get(item,"content:encoded")
        pattern = r'"(http://(?:www.)?depthfirstsearch.net/blog/wp-content/uploads/.*?)"'
        match = re.findall(pattern, content)
        if match:
            for m in match:
                attachments.add(m)
    return attachments

def search_uploads(loc):
    # search uploads for file list
    files = []
    def visit(arg, dirname, names):
        for name in names:
            full_name = dirname + "/" + name
            files.append(re.sub(loc,'http://www.depthfirstsearch.net/blog/wp-content/uploads',full_name))
            files.append(re.sub(loc,'http://depthfirstsearch.net/blog/wp-content/uploads',full_name))
    os.path.walk(loc, visit, None)
    return set(files)

def export_pages(dom, loc="pages/export"):
    pass

if True:

    fp = codecs.open('posts/wordpress/depthfirstsearch.wordpress.2013-02-20.xml', encoding='utf-8')
    dom = parse(fp)

    export_posts(dom)
   
    # Code for exploring what uploads are available. 
    # files = search_uploads("/Users/stober/wrk/web/dfs_wp/wp-content/uploads")
    # inposts = search_attachments_posts(dom)
    # asitems = export_attachments(dom)
    # missing = inposts - files 
    # # missing = inposts - asitems
    # for i in missing:
    #     print i

    # for i in inposts:
    #     print i    

    # items = list(dom.iter('item'))
    # types = set([get(item,"wp:post_type") for item in items])
    # print types

    # export_posts(dom)
    

