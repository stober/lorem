#! /usr/bin/env python
"""
Author: Jeremy M. Stober
Program: SLUG.PY
Date: Friday, January 18 2013
Description: Adapted slug code from Django.
"""
import re
import unicodedata

def slugify(value):
    """
    Converts to lowercase, removes non-word characters (alphanumerics and
    underscores) and converts spaces to hyphens. Also strips leading and
    trailing whitespace.
    """
    if type(value) == unicode:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)

if __name__ == '__main__':

    print slugify(u'This is a Test!')


