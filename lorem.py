# -*- coding: utf-8 -*-
from jinja2 import Template
import ConfigParser
import glob
import os
import codecs
import requests
import shutil
from slug import slugify
from post import Post
from blog import Blog
import PyRSS2Gen as rss
import datetime
import pdb

def process_posts(blog, index = 'id', screen = None):
    """
    Given a directory, process all posts in the directory and return a post dict.
    """
    
    locations = [os.path.expanduser(loc) for loc in blog.location]
    files = []
    for loc in locations:
        print loc
        files += glob.glob(loc + "/*.rst") 

    posts = []
    ids = set() # make sure ids are unique
    for f in files:
        post = Post()
        post.read_post(f,blog)

        if screen and not screen(post):
            pass
        elif post.id in ids:
            print "Warning: duplicate post ids found! ", f, post.id
        else:
            ids.add(post.id)
            posts.append(post)

            # bitly link creation and post fixing - TODO separate this from main processing loop
            if post.slug == "":
                print "MISSING SLUG: ", f, post.title, post.rel_link, " FIXING... "
                post.slug = slugify(post.title)
                post.write_post(fullpath=f)

            if post.link == "":
                print "MISSING LINK: ", f, post.title, post.rel_link, blog.prod_url, " FIXING..."
                post.link = "{0}/{1}".format(blog.prod_url,post.rel_link)
                post.write_post(fullpath=f)

            if post.shortlink == "":
                print "MISSING SHORTLINK: ", f, post.title, post.link, " FIXING..."
                post.shortlink = generate_shortlink(post.link, blog.plugins['bitly_login'], blog.plugins['bitly_apikey'])
                post.write_post(fullpath=f)

    return posts

def populate_posts(template, posts, blog, prev="", next = ""):
    return Template(template).render(posts=posts,blog=blog, prev=prev, next=next)

def populate_post(template, post, blog):
    """Single post."""
    return Template(template).render(post=post,blog=blog)

def generate_shortlink(url, bitly_login, bitly_apikey):
    bitly_url="http://api.bitly.com/shorten"

    params = {"login":bitly_login,
              "apiKey":bitly_apikey,
              "longUrl":url,
              "format":"json"}

    res = requests.get(bitly_url, params=params)
    shortlink = res.json()['results'][url]['shortUrl']
    return shortlink

def generate_page_url(blog,num,mkdir=True):
    if mkdir:
        mkdirs(blog, "page/{0}".format(num)) 
    return "{0}/page/{1}".format(blog.url, num)

def generate_footer(blog,num,last):
    # TODO - properly end next at last entry
    if num > 0:
        next_url = generate_page_url(blog,num-1,mkdir=False)
    prev_url = generate_page_url(blog, num+1, mkdir=True)

    if last > num > 1 :
        return '<a href="{0}">prev</a>'.format(prev_url), '<a href="{0}">next</a>'.format(next_url)
    elif num == 1:
        return '<a href="{0}">prev</a>'.format(prev_url), '<a href="{0}">next</a>'.format(blog.url)
    elif num == last:
        return '','<a href="{0}">next</a>'.format(next_url)
    elif num == 0:
        return '<a href="{0}">prev</a>'.format(prev_url),''
    else:
        raise ValueError("Bad num {0}".format(num))

def published(post):
    return post.status == 'publish'

def is_post_type(post):
    return post.type == 'post'

def is_page_type(post):
    return post.type == 'page'

def compare_dates(post1, post2):
    return cmp(post1.date, post2.date)

def mkdirs(blog,location):
    directory = "{0}/{1}".format(blog.local,location)
    try:
        os.makedirs(directory)
    except:
        pass
    return directory

def get_templates(blog):
    files = glob.glob(blog.templates + "/*.html")
    templates = {}
    for f in files:
        key = os.path.splitext(os.path.split(f)[1])[0]
        templates[key] = codecs.open(f, encoding='utf-8').read()
    return templates

def copy_support_files(blog):
    """
    Copy various support files.
    """

    styles_dir = mkdirs(blog, "styles")
    img_dir = mkdirs(blog, "img")
    uploads_dir = "{0}/{1}".format(blog.local,"uploads")
    try:
        shutil.rmtree(uploads_dir)
    except:
        pass

    shutil.copy("styles/lorem.css", styles_dir)
    shutil.copy("styles/pygments.css", styles_dir)
    shutil.copy("img/iphone_emoji.png", img_dir)
    shutil.copy("img/electric_low.png", img_dir)
    shutil.copy("img/favicon.png", img_dir)
    shutil.copytree("uploads",uploads_dir)

def chunks(l,n):
    for i in range(0,len(l),n):
        yield l[i:i+n]

def publish_feed(blog, sorted_posts):
    feed = rss.RSS2(title = blog.title,
                    description = "",
                    link = blog.url,
                    lastBuildDate = datetime.datetime.now(),
                    items = [rss.RSSItem(title=post.title,
                                         link=post.link,
                                         description= "<![CDATA[" + post.render_text + "]]>",
                                         guid=post.link,
                                         pubDate = post.date) for post in sorted_posts[:10]])
    feed.write_xml(open("_site/feed.xml","w"))

def publish_everything(blog):
    copy_support_files(blog)
    templates = get_templates(blog)
    
    # have process_posts get all the posts
    # filter published post type posts into sorted posts (for chunking)
    # filter published page type posts into separate list (to avoid chunking)

    posts = process_posts(blog, screen = published)
    regular_posts = [post for post in posts if is_post_type(post)]
    page_posts = [post for post in posts if is_page_type(post)]
    sorted_posts = sorted(regular_posts, cmp = compare_dates, reverse = True)

    pages = [(n,page) for (n,page) in enumerate(chunks(sorted_posts, 10))]

    # multipost pages
    for (i,page) in pages:
        prev,next = generate_footer(blog, i, len(pages) - 1)
        
        render = populate_posts(templates['index'], page, blog, prev, next)
        if i == 0:
            codecs.open("_site/index.html", encoding='utf-8', mode='w').write(render)
        else:
            codecs.open("_site/page/{0}/index.html".format(i), encoding='utf-8',mode='w').write(render)

    # individual post ƒ
    for post in regular_posts + page_posts:
        indv = populate_post(templates[post.type], post, blog)
        try:
            os.makedirs("_site/{0}/".format(post.rel_link))
        except:
            pass
        loc = "_site/{0}/index.html".format(post.rel_link)
        codecs.open(loc, encoding='utf-8', mode='w').write(indv)

    return sorted_posts


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description='Publish the blog.')
    parser.add_argument('--prod', action='store_true', help='Publish to production server.') 
    args = parser.parse_args()

    blog = Blog()
    blog.read_blog('blog.cfg')

    if args.prod:
        blog.url = blog.prod_url
    else:
        print "Using test url: ", blog.test_url
        blog.url = blog.test_url

    sorted_posts = publish_everything(blog)
    publish_feed(blog, sorted_posts)
