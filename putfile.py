#!/usr/bin/env python
'''
@author jstober
'''
from datetime import date
import argparse
import shutil
import os
from blog import Blog

# put a file into the uploads directory 
def main():

    parser = argparse.ArgumentParser(description='Publish a file.')
    parser.add_argument('file', help='Publish a file.') 
    args = parser.parse_args()

    loc = date.today().strftime("%Y/%m/")
    target = "uploads/{0}".format(loc)

    try:
        os.makedirs(target)
    except:
        pass

    shutil.copy(args.file, target)
    bn = os.path.basename(args.file)
    print "Location: ", "{0}{1}".format(target,bn)

    b = Blog()
    b.read_blog('blog.cfg')
    print "URL: ", "{0}/{1}{2}".format(b.prod_url, target, bn)


if __name__ == '__main__':
    main()
